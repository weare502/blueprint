(function($) {
    $(document).ready(function() {

		$(function checkForIE() {
			var isIE = false || !!document.documentMode;

			if( ! isIE ) {
				$('#is-ie').addClass('hide-this');
				return; // hide <div> and bail early
			}
			else if( isIE ) {
				$('#is-ie').dialog({
					autoOpen: true,
					modal: true,
					draggable: false,
					width: 450,
					hide: true,
					buttons: [{
						text: 'Ok',
						click: function() { $(this).dialog('close'); }
					}]
				});
			}
		});

		// $() = alias of jQuery()
		// $(function functionName() {
		// });

	}); // end doc-ready
})(jQuery)

// Non-jQuery $ can be used here (other libs)