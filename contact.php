<?php
/**
 * Template Name: Contact
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['course_schedules'] = Timber::get_posts(array(
	'post_type' => 'example',
	'posts_per_page' => 10,
	'orderby' => 'title',
	'order' => 'ASC'
));

$templates = array( 'contact.twig' );

Timber::render( $templates, $context );