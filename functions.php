<?php
// check for Timber
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

// change 'views' dir to 'templates'
Timber::$locations = __DIR__ . '/templates';

class BlueprintSite extends TimberSite {

	function __construct() {
		// Theme Support
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );

		// Action Hooks
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'enqueue_block_assets', array( $this, 'backend_frontend_styles' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'guten_editor_lockdown' ) );
		add_action( 'acf/init', array( $this, 'render_custom_acf_blocks' ) );

		// Filter Hooks
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'allowed_block_types', array( $this, 'custom_block_picker' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', array( $this, '__return_true' ) );

		parent::__construct();
	}

	function admin_head_css() {
		?><style type="text/css">
			/* Hide template mismatch warnings & make it easier to see ACF Blocks */
			/* Template mismatch is related to our lockdown function's 'empty' template - not an error */
			div.components-notice-list { display: none !important; }
			.acf-fields { border-bottom: 3px solid green !important; }
			.acf-row-handle.order { color: black !important; font-weight: bold !important; font-size: 1.2rem !important; }
		</style><?php
	}

	function enqueue_scripts() {
		// Site Styles
		wp_enqueue_style( 'blu-css', get_stylesheet_directory_uri() . "/style.css", array(), '20190416' );
		wp_enqueue_script( 'blu-js', get_template_directory_uri() . "/static/js/site-min.js", array( 'jquery', 'jquery-ui-dialog' ), '20190416' );

		// Other Deps (jQuery things)
		wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
		wp_enqueue_style( 'jquery-ui' );
	}

	// Uses the ('enqueue_block_assets') hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style.css' );
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();

		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Navigation' );

		add_image_size( 'xlarge', 2880, 2000 );
		add_image_size( 'large-square', 500, 500, true );

		acf_add_options_page( array(
			'page_title' 	=> '502 Admin',
			'menu_title'	=> 'Layout Options',
			'menu_slug' 	=> 'blu-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-example.php');
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// set what blocks are available to the block editor
	function custom_block_picker( $allowed_blocks ) {
		$allowed_blocks = array(
			// Built-in blocks
			'core/image',
			'core/heading',
			'core/paragraph',
			'core/button',
			'core/embed',
			'core-embed/facebook',
			'core-embed/youtube',
			'core-embed/twitter',

			// custom acf blocks
			'acf/three-col-grid',
		);
		return $allowed_blocks;
	}

	function guten_editor_lockdown() {
		if( ! is_admin() || ! isset( $_GET['post'] ) || '1182' !== $_GET['post'] ) {
			return false; // this is not the expected page (DO NOT LOCK)
		} else {
			$post_type_object = get_post_type_object( 'page' );
			$post_type_object->template = array(array());
	
			if( ! current_user_can('manage_options') ) {
				$post_type_object->template_lock = 'all';
			}
		}
	}
// End of 'BlueprintSite' Class
}

new BlueprintSite();

// main site nav
function blu_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu',
	));
}

// footer Menu
function blu_render_footer_menu() {
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => false,
		'menu_id' => 'footer-menu',
	));
}