<?php

$labels = array(
	'name'               => __( 'Examples', 'blu' ),
	'singular_name'      => __( 'Example', 'blu' ),
	'add_new'            => _x( 'Add New Example', 'blu', 'blu' ),
	'add_new_item'       => __( 'Add New Example', 'blu' ),
	'edit_item'          => __( 'Edit Example', 'blu' ),
	'new_item'           => __( 'New Example', 'blu' ),
	'view_item'          => __( 'View Example', 'blu' ),
	'search_items'       => __( 'Search Examples', 'blu' ),
	'not_found'          => __( 'No Examples found', 'blu' ),
	'not_found_in_trash' => __( 'No Examples found in Trash', 'blu' ),
	'parent_item_colon'  => __( 'Parent Example:', 'blu' ),
	'menu_name'          => __( 'Examples', 'blu' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => false, // make true to enable gutenberg
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-carrot',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'editor' ),
);

register_post_type( 'example', $args );