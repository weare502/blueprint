# Blueprint Theme  
  
## Blueprints is complete & ready for production! 🙃
  
## In this Update (May 6, 2019):  
- ACF RC2 pulled into build  
- Removed large portion of comments  
- Removed scaffolding  
- Removed test files / code / styles  
- Changed how Block Styles are loaded (+perf increase)  
- Added base Search Feature  
- Updated ACF Version  
- Added control for user blocks view (developer req'd to change - See [] Below)  
- Changed how the search feature is loaded + configured custom results template block  
- *Blueprints is not yet on our hosting server*  
- *Considerations - Add support for user-created reusable blocks (Block Groups, essentially)*  
  
  
### Important  
Privacy Policies / Liability Disclaimers / Cookie Policy + banner / Other logal documents should be made on Termly and embedded. [ https://termly.io ]  
  
**If you would like to submit code or a change to this repository, please checkout the `dev` branch and make a pull request.**  
  
### Plugins  
Please DO NOT add any "Super Awesome Block Builder Ultimate" Plugins. They will not work and could potentially break things.  
  
### Site Setup  
Create a new site (local or live) and select the "*Base Blueprint*" Blueprint when setting up the site.  
This will pull in all plugins and set you up with a starter theme as well as some pages and settings ready for development.  
  
### ACF Routing  
The blocks and block functions in this project are built with php and not React, though you can certainly make your own in js instead of using ACF's php approach.  
This section assumes you are using the theme as-is (or without having modified the core structure)  
- Create your block(s) settings and attributes in `acf-block-functions.php`  
	- Make SURE your blocks 'name' property and the filename are exactly the same. Timber needs this.  
- Go to `functions.php` and add your blocks 'name' to the list of allowed blocks, preceded with `acf/`  
- Next, create your twig block in `/templates/blocks/acf/` - use the name parameter you set for the name of your twig file.  
- Finally, test your block and make sure it works as intended (Editor and Frontend).  
  
  
## [ ]  
Currently, WP 5.1.1 does not have support for template targeting for blocks. The workaround I am using is to generate an 'empty' template when an Editor is using the BlockEditor where blocks have already been added to it by an admin (Required).  
  
The Editor will not be able to add any blocks or remove blocks (as none are loaded for their view). They are able to update content on those pages only. Editors can change / add / remove blocks on any page not specified in the user capabilities check.  The main check will be based on post ID; you can add more starting on Line 129 in `functions.php`.  
  
The 'New User' default role is Editor. The only Admins should be 502.  