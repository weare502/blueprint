<?php
/**
 * Displays the Search Results
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();

// Access the Post Query Object so we can loop through results
$ocntext['posts'] = new Timber\PostQuery();

global $wp_query;
$context['results_count'] = $wp_query->found_posts;
$context['get_query_string'] = get_search_query();
$context['cleanup'] = wp_reset_postdata();

$templates = array( 'search-results.twig' );

Timber::render( $templates, $context );