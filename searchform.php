<?php 

// The Search Form returned when   get_search_form() is called

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// This needs to be empty so it does not render at the top of the page
// [search-form.twig] is instead called in [base.twig]

Timber::render( $templates, $context );