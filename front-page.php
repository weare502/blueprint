<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );