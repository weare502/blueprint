<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block/

// Table of Contents
/**
 *  - 3 Column Grid : [three-col-grid]
 */

if( function_exists('acf_register_block') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks */
	$three_col_grid = array(
		'name' => 'three-col-grid',
		'title' => __( 'Three Column Grid', 'blu' ),
		'description' => __( 'Custom 3 column grid.', 'blu' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'layout',
		'align' => 'wide',
		'icon' => 'grid-view',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'three', 'column', 'grid', '3' ),
	);
	acf_register_block( $three_col_grid );

endif;